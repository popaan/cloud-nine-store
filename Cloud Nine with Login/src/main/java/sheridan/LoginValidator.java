package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		if (loginName.contains("!") || loginName.contains("?") || loginName.contains("*") || 
				loginName.contains("@") || loginName.contains("#") || loginName.contains("$") || 
				loginName.contains("%") || loginName.contains("^") || loginName.contains("&") || 
				loginName.contains("(") || loginName.contains(")") || loginName.contains("_") || 
				loginName.contains("-") || loginName.contains("=") || loginName.contains("+") || 
				loginName.contains("`") || loginName.contains("~") || loginName.contains("[") || 
				loginName.contains("]") || loginName.contains("}") || loginName.contains("{") || 
				loginName.contains(";") || loginName.contains(":") || loginName.contains("'") || 
				loginName.contains("/") || loginName.contains(",") || loginName.contains("<") || 
				loginName.contains(".") || loginName.contains(">") ||
				loginName.startsWith("1") || loginName.startsWith("2") || loginName.startsWith("3") || 
				loginName.startsWith("4") || loginName.startsWith("5") || loginName.startsWith("6") || 
				loginName.startsWith("7") || loginName.startsWith("8") || loginName.startsWith("9") || 
				loginName.startsWith("0") ||
				loginName.length() < 6) {
			return false;
		} else {
			return true;
		}
		
	}
}
